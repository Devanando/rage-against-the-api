# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    main.py                                            :+:    :+:             #
#                                                      +:+                     #
#    By: dkroeke <dkroeke@student.codam.nl>           +#+                      #
#        ehollidg <ehollidg@student.codam.nl>        +#+                       #
#    										        #+#    #+#                 #
#    License: MIT							       ########   odam.nl          #
#                                                                              #
# **************************************************************************** #


class Line:
    def __init__(self, start_loc, end_loc, start_time, end_time):
        self.start_loc = start_loc
        self.end_loc = end_loc
        self.start_time = start_time
        self.end_time = end_time

    def get_travel_time(self):
        return self.start_time.diff(self.end_time.hours, self.end_time.mins)
    