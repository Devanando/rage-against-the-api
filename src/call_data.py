# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    main.py                                            :+:    :+:             #
#                                                      +:+                     #
#    By: dkroeke <dkroeke@student.codam.nl>           +#+                      #
#        ehollidg <ehollidg@student.codam.nl>        +#+                       #
#    										        #+#    #+#                 #
#    License: MIT							       ########   odam.nl          #
#                                                                              #
# **************************************************************************** #


import requests
import json
import ptime as pt
import route as rt


class JsonData():
	def __init__(self, data):
		self.data = data


class Journey():
	def __init__(self, start, end, connection, connections):
		self.connection = connection
		self.connections = connections
		self.start = start
		self.end = end
		self.convert_medium()
		self.times = []

	def compare(self, next):
		if str(self.connection) != str(next.connection):
			return False
		return str(self.connections) == str(next.connections)

	def add_times(self, time):
		self.times.append(time)
	
	def	convert_medium(self):
		i = 0
		for i in range(0, len(self.connection)):
			self.connection[i] = medium_lookup[self.connection[i]]


def		GetAllStations():
	api_token = '58a4baf8e1db41eba897a354dfc2accf'
	api_url_base = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/stations'
	url = api_url_base + '?Subscription-Key={0}'.format(api_token)
	data = requests.get(url).text
	return json.loads(data)


def		GetCodeLookUp():
	lookups = "{"
	i = 0
	for station in all_stations['payload']:
		lookups += "\"" + station['namen']['middel'] + "\":\"" + station['code'] + "\",\n"
		lookups += "\"" + station['code'] + "\":\"" + station['namen']['middel'] + "\",\n"
		i += 1
	lookups = lookups[:-2]
	lookups += '}'
	return(json.loads(lookups))


def		GetMediumLookUp():
	lookups = "{"
	i = 0
	for station in all_stations['payload']:
		lookups += "\"" + station['namen']['lang'] + "\":\"" + station['namen']['middel'] + "\",\n"
		lookups += "\"" + station['namen']['middel'] + "\":\"" + station['namen']['middel'] + "\",\n"
		i += 1
	lookups = lookups[:-2]
	lookups += '}'
	return(json.loads(lookups))


all_stations = GetAllStations()
code_lookup = GetCodeLookUp()
medium_lookup = GetMediumLookUp()


def		get_suggestions(names):
	def		get_suggestions(query):
		formatted, unformatted = list(), list()
		if query:
			for station in names['payload']:
				if query in station['namen']['middel']:
					formatted.append(station['namen']['middel'])
					unformatted.append(station['namen']['middel'])
		return formatted, unformatted
	return get_suggestions


def		get_connections(data):
	destination = None
	array_route = []
	array_routes = []
	i = 0

	for directions in data['payload']['departures']:
		for direction in directions.items():
			for items in direction:
				if i == 1:
					stations = []
					for station in items:
						stations.append(station['mediumName'])
					stations.append(destination)
					destination = None
					array_routes.append(stations)
					i = 0
				if i == 2:
					destination = items
					i = 0
				if items == "routeStations":
					i = 1
				if items == "direction":
					i = 2
	return array_routes


def		load_json(station, api):
	api_token = '58a4baf8e1db41eba897a354dfc2accf'
	url_base = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/'
	url_base = url_base + "{}".format(api)
	url = url_base + '?Subscription-Key={0}'.format(api_token)
	url = url + '&station={}'.format(station)
	data = requests.get(url).text
	data = json.loads(data)
	return data


def		get_origin(data):
	originz = []
	i = 0
	for origin in data['payload']['arrivals']:
		for origins in origin.items():
			for items in origins:
				if i == 1:
					originz.append(items)
					i = 0
				if items == 'origin':
					i = 1
	return originz


def		find_route_sec(start, end):
	
	arrivals_data = load_json(end, "departures")
	departure_data = load_json(start, "departures")
	connections_data = get_connections(departure_data)
	origin_data = get_connections(arrivals_data)
	thisdict = {}
	k = 0
	for i in range(0, len(connections_data)):
		y = 0
		for y in range(0, len(connections_data[i])):
			x = 0
			for x in range(0, len(origin_data)):
				z = 0
				for z in range(0, len(origin_data[x])):
					c_var = medium_lookup[connections_data[i][y]]
					if (origin_data[x][z] == c_var):
						journey = Journey(code_lookup[start], code_lookup[end], connections_data[i], origin_data[x])
						if not dict_contains('route', k, thisdict, journey):
							thisdict['route' + str(k)] = journey
							k += 1
	final_journey = compare_times(thisdict, 'route', k)
	return rt.route_from_journey(final_journey)


def		time_from_journey(journey):
	url = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v3/trips?'
	url += 'Subscription-Key=' + '58a4baf8e1db41eba897a354dfc2accf'
	url += '&fromStation=' + journey.start + '&toStation=' + journey.end
	url += '&viaStation=' + code_lookup[journey.connection[0]]
	js = json.loads(requests.get(url).text)
	d_time = pt.str_to_time(js['trips'][0]['legs'][0]['origin']['plannedDateTime'])
	a_time = pt.str_to_time(js['trips'][0]['legs'][0]['destination']['plannedDateTime'])
	try:
		sa_time = pt.str_to_time(js['trips'][0]['legs'][0]['stops'][0]['plannedArrivalDateTime'])
	except:
		sa_time = pt.str_to_time(js['trips'][0]['legs'][0]['stops'][0]['plannedDepartureDateTime'])
	sd_time = pt.str_to_time(js['trips'][0]['legs'][0]['stops'][0]['plannedDepartureDateTime'])
	journey.add_times(a_time)
	if sa_time != None:
		journey.add_times(sa_time)
		journey.add_times(sd_time)
	journey.add_times(d_time)
	return journey.times[0].diff(journey.times[-1].hours, journey.times[-1].mins)


def		compare_times(thisdict, dict_str, max_index):
	i = 0
	time = None
	journey = None
	while i < max_index:
		tm = time_from_journey(thisdict[dict_str + str(i)])
		if time != None and time.is_bigger(tm):
			time = tm
			journey = thisdict[dict_str + str(i)]
		elif time == None:
			time = tm
			journey = thisdict[dict_str + str(i)]
		i += 1
	return journey


def		dict_contains(dict_str, max_index, thisdict, journey):
	i = 0
	while i < max_index:
		if thisdict[dict_str + str(i)].compare(journey) == True:
			return True
		i += 1
	return False


def		find_route_first(start, end):

	data = load_json(start, "departures")
	data = get_connections(data)
	data_end = None
	length_routes = len(data)
	for i in range(0, length_routes):
		x = 0
		length_line = len(data[i])
		for x in range(0, length_line):
			if data[i][x] == end:
				return data[i]
			x += 1
		i += 1
	return None
