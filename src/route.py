# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    main.py                                            :+:    :+:             #
#                                                      +:+                     #
#    By: dkroeke <dkroeke@student.codam.nl>           +#+                      #
#        ehollidg <ehollidg@student.codam.nl>        +#+                       #
#    										        #+#    #+#                 #
#    License: MIT							       ########   odam.nl          #
#                                                                              #
# **************************************************************************** #


import ptime as pt
import line
import wx.dataview
import wx


class Route:
    def __init__ (self):
        self.lines = []

    def get_travel_time(self):
        time = pt.Time()
        for line in self.lines:
            tm = line.get_travel_time()
            time.add(tm.hours, tm.mins)
        return time

    def print_to_dataview(self, dataview):
        dataview.Clear()
        clms = dataview.GetColumns()
        for ln in self.lines:
            clms[0].AddValue(ln.start_loc)
            clms[1].AddValue(ln.start_time.ret_str())
            clms[2].AddValue(ln.end_loc)
            clms[3].AddValue(ln.end_time.ret_str())


def route_from_journey(journey):
    i = 0
    rt = Route()
    max = len(journey.times)
    while i < max:
        start = None
        end = None
        if i == 0:
            start = journey.start
        else:
            start = journey.connection[int(i / 2) - 1]
        if i == max - 2:
            end = journey.end
        else:
            end = journey.connection[int(i / 2) - 1]
        rt.lines.append(line.Line(start, end, journey.times[i], journey.times[i + 1]))
        i += 2
    return rt

