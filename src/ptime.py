# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    main.py                                            :+:    :+:             #
#                                                      +:+                     #
#    By: dkroeke <dkroeke@student.codam.nl>           +#+                      #
#        ehollidg <ehollidg@student.codam.nl>        +#+                       #
#    										        #+#    #+#                 #
#    License: MIT							       ########   odam.nl          #
#                                                                              #
# **************************************************************************** #


class Time:

    def __init__(self, hours = 0, mins = 0):
        self.hours = hours
        self.mins = mins
        self.equalize()

    def is_bigger(self, next):
        if self.hours > next.hours:
            return True
        elif self.hours < next.hours:
            return False
        elif self.mins > next.mins:
            return True
        return False

    def str_to_time(self, str_time):
        time = str_time.split("T")[1]
        hrs = int(time[:2])
        mns = int(time[2:4])
        self.hours = hrs
        self.mins = mns
        self.equalize()

    def add(self, hours, mins):
        self.hours += hours
        self.mins += mins
        self.equalize()

    def sub(self, hours, mins):
        self.hours -= hours
        self.mins -= mins
        self.equalize()

    def equalize(self):
        while self.mins > 59:
            self.mins -= 60
            self.hours += 1
        while self.hours > 23:
            self.hours -= 24
        while self.mins < 0:
            self.mins += 60
            self.hours -= 1
        while self.hours < 0:
            self.hours += 24

    def diff(self, hours, mins):
        s_dec = time_decimal(self.hours, self.mins)
        n_dec = time_decimal(hours, mins)
        f_dec = s_dec - n_dec
        f_dec = abs(f_dec)
        return (decimal_time(f_dec))

    def ret_str(self):
        if self.mins < 10 and self.hours < 10:
            return ("0" + str(self.hours) +  ":0" + str(self.mins))
        if self.mins < 10:
            return (str(self.hours) +  ":0" + str(self.mins))
        if self.hours < 10:
            return ("0" + str(self.hours) +  ":" + str(self.mins))
        return (str(self.hours) +  ":" + str(self.mins))


def str_to_time(time_str):
    if time_str == None:
        return None
    t_str = time_str.split('T')[1].split(':')[:-1]
    return Time(hours=int(t_str[0]), mins=int(t_str[0]))


def decimal_time(dec):
    hours = 0
    while dec > 59:
        dec -= 60
        hours += 1
    return (Time(hours, dec))


def hours_decimal(hours):
    return (hours * 60)


def time_decimal(time):
    time_decimal(time.hours, time.mins)


def time_decimal(hours, mins):
    return (hours_decimal(hours) + mins)
