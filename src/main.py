#!/usr/bin/env python3

# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    main.py                                            :+:    :+:             #
#                                                      +:+                     #
#    By: dkroeke <dkroeke@student.codam.nl>           +#+                      #
#        ehollidg <ehollidg@student.codam.nl>        +#+                       #
#    										        #+#    #+#                 #
#    License: MIT							       ########   odam.nl          #
#                                                                              #
# **************************************************************************** #

import wx
import call_data
import gui
import route as rot
import line as ln
import ptime as pt


if __name__ == "__main__":
	app = wx.App()	
	frame = gui.MainFrame(None,
		title='Codam NS Traveller', size=wx.Size(800, 800),
		style=wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.CLIP_CHILDREN | wx.ICONIZE |
		wx.MINIMIZE_BOX)
	frame.Show()
	app.MainLoop()
