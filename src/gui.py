# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    main.py                                            :+:    :+:             #
#                                                      +:+                     #
#    By: dkroeke <dkroeke@student.codam.nl>           +#+                      #
#        ehollidg <ehollidg@student.codam.nl>        +#+                       #
#    										        #+#    #+#                 #
#    License: MIT							       ########   odam.nl          #
#                                                                              #
# **************************************************************************** #


import wxautocompletectrl as acc
import wx
import wx.adv
import wx.dataview
import call_data
import route as rot
import line as ln
import ptime as pt


class Column(wx.Panel):
	def __init__(self, parent, header, width, *args, **kwargs):
		wx.Panel.__init__(self, parent, *args, **kwargs)
		self.parent = parent
		self.Sizer = wx.BoxSizer(wx.VERTICAL)
		self.width = width
		self.header = header
		self.Sizer.Add(wx.TextCtrl(self, style=wx.TE_READONLY | wx.TE_CENTRE,
			value=header, size=wx.Size(width, 20)))
		self.Refresh()

	def AddValue(self, val):
		self.Sizer.Add(wx.TextCtrl(self, style=wx.TE_READONLY | wx.TE_CENTRE,
			value=val, size=wx.Size(self.width, 20)), 0, wx.EXPAND)
		self.Sizer.Layout()
		self.Parent.Sizer.Layout()
		self.Parent.Parent.Sizer.Layout()
		self.Refresh()

	def RemoveValue(self, index):
		self.Sizer.Remove(i + 1)
		self.Sizer.Layout()
		self.Parent.Sizer.Layout()
		self.Parent.Parent.Sizer.Layout()
		self.Refresh()
	
	def Clear(self):
		self.Sizer.Clear(True)
		self.Sizer.Add(wx.TextCtrl(self, style=wx.TE_READONLY | wx.TE_CENTRE,
			value=self.header, size=wx.Size(self.width, 20)))
		self.Sizer.Layout()
		self.Parent.Sizer.Layout()
		self.Parent.Parent.Sizer.Layout()
		self.Refresh()


class Output_panel(wx.Panel):
	def __init__(self, parent, *args, **kwargs):
		wx.Panel.__init__(self, parent, *args, **kwargs)
		self.parent = parent
		self.c0 = None
		self.c1 = None
		self.c2 = None
		self.c3 = None
		self.SetColumns()

	def SetColumns(self):
		self.Sizer = wx.BoxSizer(wx.HORIZONTAL)
		self.c0 = Column(self, "Departure Location", 200)
		self.c1 = Column(self, "Departure Time", 150)
		self.c2 = Column(self, "Arrival Location", 200)
		self.c3 = Column(self, "Arrival Time", 150)
		self.Sizer.Add(self.c0, 0, wx.CENTER)
		self.Sizer.Add(self.c1, 0, wx.CENTER)
		self.Sizer.Add(self.c2, 0, wx.CENTER)
		self.Sizer.Add(self.c3, 0, wx.CENTER)

	def RemoveRow(index):
		self.c0.RemoveValue(index)
		self.c1.RemoveValue(index)
		self.c2.RemoveValue(index)
		self.c3.RemoveValue(index)

	def Clear(self):
		self.c0.Clear()
		self.c1.Clear()
		self.c2.Clear()
		self.c3.Clear()

	def GetColumn(self, i):
		if i == 0:
			return self.c0
		elif i == 1:
			return self.c1
		elif i == 2:
			return self.c2
		else:
			return self.c3

	def GetColumns(self):
		return [self.c0, self.c1, self.c2, self.c3]


class Input_panel(wx.Panel):

	def __init__(self, parent, *args, **kwargs):
		wx.Panel.__init__(self, parent, *args, **kwargs)
		self.parent = parent
		self.Sizer = wx.BoxSizer(wx.VERTICAL)
		self.inputs = []
		self.Sizer.Add(wx.StaticText(self))
		self.Input('To')
		self.Sizer.Add(wx.StaticText(self))
		self.Input('From')
		self.Sizer.Add(wx.StaticText(self))
		self.btn = wx.Button(self, -1, label="Get route")
		self.Sizer.Add(self.btn, 0, wx.CENTER)
		self.Sizer.Add(wx.StaticText(self))
		self.out = None
		self.Output()
		self.btn.Bind(wx.EVT_BUTTON, lambda e: OnClick(e, self))
		self.Refresh()

	def Input(self, labelz):
		sizer = wx.BoxSizer(wx.HORIZONTAL)
		sizer.Add(wx.StaticText(self, -1, label=labelz), 2, 0, 0)
		self.inputs.append(acc.AutocompleteTextCtrl(self, completer=call_data.get_suggestions(call_data.all_stations)))
		sizer.Add(self.inputs[-1], 2, 0, 0)
		sizer.SetSizeHints(self)
		self.Sizer.Add(sizer, 0, wx.CENTER)

	def Output(self):
		self.out = Output_panel(self)
		self.Sizer.Add(self.out, 0, wx.CENTER)


class MainFrame(wx.Frame):

	def __init__(self, *args, **kwargs):
		wx.Frame.__init__(self, *args, **kwargs)
		self.Panel = Input_panel(self)


def OnClick(e, in_panel):
	from_loc = call_data.code_lookup[in_panel.inputs[0].GetValue()]
	to_loc = call_data.code_lookup[in_panel.inputs[1].GetValue()]
	rt = call_data.find_route_first(from_loc, to_loc)
	if rt == None:
		rt = call_data.find_route_sec(from_loc, to_loc)
	if rt != None:
		rt.print_to_dataview(in_panel.out)
	else:
		print("Route Not Supported")
